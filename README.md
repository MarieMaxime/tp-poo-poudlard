# TP POO Poudlard

##UMLs

les UML sont dispo dans le projet en image ou en format drawio

## lancer le programme:
        
        npm i
        npm run dev

le resultat se trouve dans la console du navigateur, j'ai chercher pour pouvoir afficher un truc plus pratique mais j'ai pas trouvé grand chose


## Ennoncé
###Avant-propos :
Je sais que vous êtes tous très occupés au travails, MAIS d'une décision commune avec l'équipe pédagogique, nous
avons décidé de rendre ce TP obligatoire afin de suivre votre évolution depuis la dernière évaluation de POO.
Je pense qu’il y en a pour 3 à 4 heures de travail grand maximum. Ce TP sera noté pour pouvoir évaluer votre
progression avec tout ce qu'on a fait dans la semaine. Sur ce bon courage !
Cordialement Nathanaël !
###Problématique :
Nouvellement nommée directrice de l'école de magie Poudlard, Hermione GRANGER souhaite moderniser l'école en
adoptant des technologies moldues. Sa première mesure est de mettre en place un système informatique pour gérer
les différentes informations de l'école ! Et pour ce faire, elle fait appel à vous !
Les Informations
- Poudlard à quatre maisons : Gryffondor, Serpentard, Poufsouffle, Serdaigle
- Poudlard à une directrice, et une directrice seulement.
- Poudlard à plusieurs professeurs, un professeur ne peut avoir qu’une matière.
- Les matières possibles sont les suivantes : Potions, Rune, Astronomie, Défense contre les forces du mal,
  Sortilèges, Divination, Botanique, Soins aux créatures magique.
- Chaque maison à un directeur de maison, qui est unprofesseur.
- Chaque maison à un nombre de points pour la coupe des maisons, ces points évoluent en fonction
  des actions commises par les élèves.
- Chaque maison à deux préfets.
- N'importe quel individu, qu'il soit directeur, professeur ou encore élève appartient à une maison ... Après tout, tout
  le monde est formé ou a été formé à Poudlard !
- La formation de Poudlard se déroule sur sept ans.
- Chaque élève est dans une année. (Exemple : « Première année » « Deuxième année » etc.)
- Le directeur peut renvoyer quelqu'un.
- Le directeur peut ajouter ou soustraire des points à la maison d'un élève, entre -250 et 200 maximum à chaque
  fois.
- Le directeur peut nommer un directeur de maison.
- Le directeur peut donner des retenus.
- Les professeurs peuvent ajouter ou soustraire des points à la maison d'un élève entre -100 et 100 maximum à
  chaque fois.
- Les professeurs peuvent donner des cours.
- Les professeurs peuvent donner des retenus.
- Un préfet est aussi un élève.
- Les préfets peuvent eux aussi ajouter ou soustraire des points, mais moins entre -25 et 25 maximum à chaque
  fois.
- Les préfets sont nommés chaque année, entre leur cinquième et septième année, et une fois en septième année,
  ils peuvent être nommée préfet en chef.
- Seul deux préfets en chef sont nommés et ils peuvent ajouter ou soustraire des points entre -50 et 50 maximum
  à chaque fois.
  Il y a donc les huit préfets des maisons et deux préfets en chef.
 ### ATTENTION !
  Les deux préfets des maisons sont forcément un garçon et une fille, de même pour les préfets en
  chef : un garçon et une fille.
  Chaque année de nouveaux élèves arrivent à Poudlard et ils sont répartis dans les maisons par le Choixpeau !
  ###Objectifs :
  Votre objectif est d'aider Hermione dans sa mission, en utilisant les informations qu'elle vous a donné plus haut !
  ##Réalisation :
  ###UML :
  Diagramme de cas d'utilisation
  Diagramme de classe
  ###Code :
  Réaliser les différentes classes nécessaires pour modéliser les différentes informations. Réaliser de
  quoi afficher les informations dans une console.
  ###Autres :
  Si vous souhaitez rajouter votre grain de sel en ajoutant des choses comme les fantômes, les salles de classe
  et autres éléments qui vous inspirent, n'hésitez pas !
  Voir le barème en Annexe
