import './style.css'
import {Ecole} from "./Classes/Ecole";
import {Maison} from "./Classes/Maison";
import {GenreEnum, MatiereEnum, NomMaisonEnum} from "./Classes/poudlard.model";
import {Directrice} from "./Classes/Directrice";
import {Professeur} from "./Classes/Professeur";

const app = document.querySelector<HTMLDivElement>('#app')!

app.innerHTML = `
  <div id="test"></div>
`
let pointsMaison = 1000;

let poudlard = new Ecole("Poudlard")

let serpentard = new Maison(NomMaisonEnum.serpentard, pointsMaison)
poudlard.addMaison(serpentard)
let griffondor = new Maison(NomMaisonEnum.griffondor, pointsMaison)
poudlard.addMaison(griffondor)
let poufsouffle = new Maison(NomMaisonEnum.poufsouffle, pointsMaison)
poudlard.addMaison(poufsouffle)
let serdaigle = new Maison(NomMaisonEnum.serdaigle, pointsMaison)
poudlard.addMaison(serdaigle)

let hermione = new Directrice("Hermione", NomMaisonEnum.griffondor, GenreEnum.femelle, 7)
let rogue = new Professeur("Rogue", NomMaisonEnum.serpentard, GenreEnum.male, 7, {nomMatiere: MatiereEnum.potion})

poudlard.setDirectrice(hermione)
poudlard.setProfesseur(rogue)
poudlard.addEleve("harry", 3, GenreEnum.male)

hermione.nommerPrefet("harry")
hermione.nommerDirecteurDeMaison("Rogue", NomMaisonEnum.serpentard)

console.dir(poudlard)

