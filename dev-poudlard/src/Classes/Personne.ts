import {GenreEnum, NomMaisonEnum} from "./poudlard.model";

export abstract class Personne {

    nomPersonne: string;
    maison: NomMaisonEnum;
    genre: GenreEnum;
    anneeDEtude: number;

    constructor(nomPersonne: string, maison: NomMaisonEnum, genre: GenreEnum, anneeDEtude: number) {
        this.nomPersonne = nomPersonne;
        this.maison = maison;
        this.genre = genre;
        this.anneeDEtude = anneeDEtude;
    }

    abstract ajouterPoint(nbPoints: number, maison: NomMaisonEnum): void
    abstract soustrairePoint(nbPoints: number, maison: NomMaisonEnum): void

}
