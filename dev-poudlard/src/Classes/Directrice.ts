import {Personne} from "./Personne";
import {GenreEnum, NomMaisonEnum} from "./poudlard.model";
import {Administration} from "./Administration";

export class Directrice extends Personne {

    maxPoints: number = 250;
    administration: Administration | undefined

    constructor(nom: string, maison: NomMaisonEnum, genre: GenreEnum, anneeDEtude: number) {
        super(nom, maison, genre, anneeDEtude);
    }

    setAdministration(administration: Administration): void {
        this.administration = administration;
    }

    ajouterPoint(nbPoints: number, maison: NomMaisonEnum): void {
        if (nbPoints < this.maxPoints) {
            this.administration?.ajouterPoints(nbPoints, maison)
        } else {
            console.log("vous pouvez ajouter un maximum de " + this.maxPoints + "points")
        }
    }

    soustrairePoint(nbPoints: number, maison: NomMaisonEnum): void {
        if (nbPoints > this.maxPoints) {
            this.administration?.soustrairePoints(nbPoints, maison)
        } else {
            console.log("vous pouvez enlever un maximum de " + this.maxPoints + "points")
        }
    }

    renvoyerEleve(nom: string): void {
        this.administration?.renvoyerEleve(nom);
    }

    nommerDirecteurDeMaison(futurDirecteur: string, maison: NomMaisonEnum): void {
        if (futurDirecteur) {
            this.administration?.setDirecteurDeMaison(futurDirecteur, maison)
            console.log("le professseur " + futurDirecteur + " est maintenant le directeur de " + maison);
        }
    }

    donnerRetenue(nom: string): void {
        this.administration?.donnerRetenue(nom)
    }

    nommerPrefet(nom: string): void {
        this.administration?.nommerPrefet(nom)
    }

    nommerPrefetEnChef(nom: string): void {
        this.administration?.nommerPrefetEnChef(nom)
    }
}
