import {GenreEnum, NomMaisonEnum} from "./poudlard.model";
import {Eleve} from "./Eleve";
import {Administration} from "./Administration";

export class PrefetEnChef extends Eleve {

    maxPoints: number = 25;
    administration: Administration | undefined

    constructor(nom: string, maison: NomMaisonEnum, genre: GenreEnum, anneeDEtude: number) {
        super(nom, maison, genre, anneeDEtude);
    }

    setAdministration(administration: Administration) {
        this.administration = administration;
    }

    ajouterPoint(nbPoints: number, maison: NomMaisonEnum): void {
        if (nbPoints < this.maxPoints) {
            this.administration?.ajouterPoints(nbPoints, maison)
        } else {
            console.log("vous pouvez ajouter un maximum de " + this.maxPoints + "points")
        }
    }

    soustrairePoint(nbPoints: number, maison: NomMaisonEnum): void {
        if (nbPoints > this.maxPoints) {
            this.administration?.soustrairePoints(nbPoints, maison)
        } else {
            console.log("vous pouvez enlever un maximum de " + this.maxPoints + "points")
        }
    }
}
