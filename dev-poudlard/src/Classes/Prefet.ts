import {Eleve} from "./Eleve";
import {GenreEnum, NomMaisonEnum} from "./poudlard.model";

export class Prefet extends Eleve {

    constructor(nom: string, maison: NomMaisonEnum, genre: GenreEnum, anneeDEtude: number) {
        super(nom, maison, genre, anneeDEtude);
    }
}
