import {Personne} from "./Personne";
import {GenreEnum, NomMaisonEnum} from "./poudlard.model";

export class Eleve extends Personne{

    anneeDEtude: number;

    constructor(nom: string, maison: NomMaisonEnum, genre: GenreEnum, anneeDEtude: number) {
        super(nom, maison, genre, anneeDEtude);
        this.anneeDEtude = anneeDEtude
    }

    ajouterPoint(_: number, __: NomMaisonEnum): void {
        console.log("tu n'as pas le droit")
    }

    soustrairePoint(_: number, __: NomMaisonEnum): void {
        console.log("tu n'as pas le droit")
    }
}
