import {NomMaisonEnum} from "./poudlard.model";

export class Maison {

    nom: NomMaisonEnum;
    directeurDeMaison: string | undefined;
    pointsMaison: number;
    prefet1: string | undefined;
    prefet2: string | undefined;

    constructor(nom: NomMaisonEnum, pointsMaison: number) {
        this.nom = nom;
        this.pointsMaison = pointsMaison
    }

    setPrefet1(nom: string): void {
        this.prefet1 = nom
    }

    setPrefet2(nom: string): void {
        this.prefet2 = nom
    }

}
