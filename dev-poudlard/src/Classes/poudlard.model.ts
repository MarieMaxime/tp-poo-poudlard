export enum GenreEnum {
    male= "male",
    femelle = "femelle",
}

export enum MatiereEnum {
    potion,
    rune,
    astronomie,
    defenceContreLesForcesDuMal,
    sortilege,
    divination,
    botanique,
    soinsAuCreaturesMagique,
}

export enum NomMaisonEnum {
    griffondor = "Griphondor",
    serpentard = "Serpentard",
    poufsouffle = "Poufsouffle",
    serdaigle = "Serdaigle",
}
