import {Matiere} from "./Matiere";
import {Personne} from "./Personne";
import {GenreEnum, NomMaisonEnum} from "./poudlard.model";
import {Administration} from "./Administration";

export class Professeur extends Personne {

    maxPoints: number = 100;
    matiere: Matiere;
    administration: Administration | undefined

    constructor(nom: string, maison: NomMaisonEnum, genre: GenreEnum, anneeDEtude: number, matiere: Matiere) {
        super(nom, maison, genre, anneeDEtude);
        this.matiere = matiere;
    }

    setAdministration(administration: Administration): void {
        this.administration = administration;
    }

    setMatiere(matiere: Matiere): void {
        this.matiere = matiere
    }

    donnerCours(annee: number): void {
        this.administration?.getPromo(annee)
    }

    donnerRetenue(nom: string): void {
        this.administration?.donnerRetenue(nom)
    }

    ajouterPoint(nbPoints: number, maison: NomMaisonEnum): void {
        if (nbPoints < this.maxPoints) {
            this.administration?.ajouterPoints(nbPoints, maison)
        } else {
            console.log("vous pouvez ajouter un maximum de " + this.maxPoints + "points")
        }
    }

    soustrairePoint(nbPoints: number, maison: NomMaisonEnum): void {
        if (nbPoints > this.maxPoints) {
            this.administration?.soustrairePoints(nbPoints, maison)
        } else {
            console.log("vous pouvez enlever un maximum de " + this.maxPoints + "points")
        }
    }
}
