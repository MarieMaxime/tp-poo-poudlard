import {GenreEnum, NomMaisonEnum} from "./poudlard.model";
import {Maison} from "./Maison";
import {Personne} from "./Personne";
import {Prefet} from "./Prefet";
import {Choixpeau} from "./Choixpeau";
import {Eleve} from "./Eleve";

export class Administration {

    personneList: Map<string, Personne>;
    maison: Map<NomMaisonEnum, Maison>

    constructor() {
        this.personneList = new Map();
        this.maison = new Map();
    }

    addMaison(maison: Maison): void {
        this.maison.set(maison.nom, maison)
    }

    addEleve(nom: string, anneeDEtude: number, genre: GenreEnum): void {
        let eleve = new Eleve(nom, Choixpeau.repartirDansMaison(), genre, anneeDEtude)
        this.personneList.set(nom, eleve)
    }

    renvoyerEleve(nom: string): void {
        if (this.personneList.has(nom)) {
            this.personneList.delete(nom)
        } else {
            console.log("cet eleve n'existe pas")
        }
    }

    donnerRetenue(nom: string): void {
        if (this.personneList.has(nom)) {
            console.log("l'élève " + nom + " est en retenue!")
        } else {
            console.log("cet eleve n'existe pas")
        }
    }

    nommerPrefet(nom: string): void {
        let anneeDEtude = this.personneList.get(nom)?.anneeDEtude
        let nomPersonne = this.personneList.get(nom)?.nomPersonne
        let genre = this.personneList.get(nom)?.genre
        let maison = this.personneList.get(nom)?.maison
        if (this.personneList.has(nom) && anneeDEtude && anneeDEtude >= 6) {
            let prefet = new Prefet(nomPersonne!, maison!, genre!, anneeDEtude!)
            this.personneList.delete(nom)
            this.personneList.set(prefet.nomPersonne, prefet)
            this.maison.get(prefet.maison)?.setPrefet1(prefet.nomPersonne)
        }
    }

    nommerPrefetEnChef(nom: string): void {
        let anneeDEtude = this.personneList.get(nom)?.anneeDEtude
        let nomPersonne = this.personneList.get(nom)?.nomPersonne
        let genre = this.personneList.get(nom)?.genre
        let maison = this.personneList.get(nom)?.maison
        if (this.personneList.has(nom) && anneeDEtude && anneeDEtude >= 7) {
            let prefet = new Prefet(nomPersonne!, maison!, genre!, anneeDEtude!)
            this.personneList.delete(nom)
            this.personneList.set(prefet.nomPersonne, prefet)
        }
    }

    getPromo(annee: number): Personne[] {
        let personnes = this.personneList.values()
        let promoArray = []
        for (let personne of personnes) {
            if (personne.anneeDEtude === annee) {
                promoArray.push(personne)
            }
        }
        return promoArray
    }

    ajouterPoints(nbPoints: number, maison: NomMaisonEnum): void {
        this.maison.get(maison)!.pointsMaison += nbPoints;
    }

    soustrairePoints(nbPoints: number, maison: NomMaisonEnum): void {
        this.maison.get(maison)!.pointsMaison -= nbPoints;
    }

    setDirecteurDeMaison(nom: string, maison: NomMaisonEnum): void {
        this.maison.get(maison)!.directeurDeMaison = nom
    }
}
