import {MatiereEnum} from "./poudlard.model";

export interface Matiere {

    nomMatiere: MatiereEnum;

}
