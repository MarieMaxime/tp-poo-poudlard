import {Directrice} from "./Directrice";
import {Professeur} from "./Professeur";
import {Administration} from "./Administration";
import {GenreEnum} from "./poudlard.model";
import {Maison} from "./Maison";

export class Ecole {

    nom: string;
    directrice: Directrice | undefined;
    professeurList: Professeur[] | undefined;
    departementAdministration: Administration;

    constructor(nom: string) {
        this.nom = nom;
        this.departementAdministration = new Administration()
    }

    setDirectrice(directrice: Directrice) {
        this.directrice = directrice
        this.directrice.setAdministration(this.departementAdministration)
    }

    setProfesseur(professeur: Professeur) {
        professeur.setAdministration(this.departementAdministration)
        this.professeurList?.push(professeur)
    }

    addEleve(nom: string, anneeDEtude: number, genre: GenreEnum) {
        this.departementAdministration.addEleve(nom, anneeDEtude, genre)
    }

    addMaison(maison: Maison) {
        this.departementAdministration.addMaison(maison)
    }


}
