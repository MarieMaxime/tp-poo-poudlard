import {NomMaisonEnum} from "./poudlard.model";

export class Choixpeau {

    static repartirDansMaison(): NomMaisonEnum {
        return randomEnumKey()
    }
}

const randomEnumKey = function(): NomMaisonEnum {
    const keys = Object.keys(NomMaisonEnum)
        .filter(k => !(Math.abs(Number.parseInt(k)) + 1));
    const enumKey = keys[Math.floor(Math.random() * keys.length)];
    return enumKey as NomMaisonEnum;
};
